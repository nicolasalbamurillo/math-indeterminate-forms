package org.fjala.math.indeterminateforms;

import org.junit.jupiter.api.Test;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

public class InderFormsGetterTest {

    @Test
    public void testSomeMathMethods() {
        assertThat(Math.pow(0, 0), not(equalTo(Double.NaN)));
        assertThat(Math.pow(0, 0), equalTo(1d));

        assertThat(Math.pow(Double.POSITIVE_INFINITY, 0), not(equalTo(Double.NaN)));
        assertThat(Math.pow(Double.POSITIVE_INFINITY, 0), equalTo(1d));

        assertThat(Math.pow(Double.NaN, 0), not(equalTo(Double.NaN)));
        assertThat(Math.pow(Double.NaN, 0), equalTo(1d));
    }

    @Test
    public void testDoubleFiniteMethod() {
        assertThat(Double.isFinite(Double.POSITIVE_INFINITY), is(false));
        assertThat(Double.isFinite(Double.NEGATIVE_INFINITY), is(false));
        assertThat(Double.isFinite(Double.NaN), is(false));

        assertThat(Double.isFinite(0d), is(true));
    }

    @Test
    public void tesDoubleIsInfinityMethod(){
        assertThat(Double.isInfinite(Double.POSITIVE_INFINITY), is(true));
        assertThat(Double.isInfinite(Double.NEGATIVE_INFINITY), is(true));
        assertThat(Double.isInfinite(Double.NaN), is(false));

        assertThat(Double.isInfinite(0d), is(false));
    }

    @Test
    public void tesDoubleIsNaNMethod(){
        assertThat(Double.isNaN(Double.POSITIVE_INFINITY), is(false));
        assertThat(Double.isNaN(Double.NEGATIVE_INFINITY), is(false));
        assertThat(Double.isNaN(Double.NaN), is(true));

        assertThat(Double.isNaN(0d), is(false));
    }

    @Test
    public void testFirstForm() {
        InderFormsGetter inderForms = new InderFormsGetter();

        double result = inderForms.getFirstForm();

        assertThat(result, equalTo(Double.NaN));
    }

    @Test
    public void testSecondForm() {
        InderFormsGetter inderForms = new InderFormsGetter();

        double result = inderForms.getSecondForm();

        assertThat(result, equalTo(Double.NaN));
    }

    @Test
    public void testThirdForm() {
        InderFormsGetter inderForms = new InderFormsGetter();

        double result = inderForms.getThirdForm();

        assertThat(result, equalTo(Double.NaN));
    }

    @Test
    public void testFourthForm() {
        InderFormsGetter inderForms = new InderFormsGetter();

        double result = inderForms.getFourthForm();

        assertThat(result, equalTo(Double.NaN));
    }

    @Test
    public void testFifthForm() {
        InderFormsGetter inderForms = new InderFormsGetter();

        double result = inderForms.getFifthForm();

        assertThat(result, equalTo(Double.NaN));
    }

    @Test
    public void testSixthForm() {
        InderFormsGetter inderForms = new InderFormsGetter();

        double result = inderForms.getSixthForm();

        assertThat(result, equalTo(Double.NaN));
    }

    @Test
    public void testSeventhForm() {
        InderFormsGetter inderForms = new InderFormsGetter();

        double result = inderForms.getSeventhForm();

        assertThat(result, equalTo(Double.NaN));
    }

    @Test
    public void testGenerateAndGetPositiveInfinity() {
        InderFormsGetter inderForms = new InderFormsGetter();

        double result = inderForms.generateAndGetPositiveInfinity();

        assertThat(result, equalTo(Double.POSITIVE_INFINITY));
    }

    @Test
    public void testGenerateAndGetNegativeInfinity() {
        InderFormsGetter inderForms = new InderFormsGetter();

        double result = inderForms.generateAndGetNegativeInfinity();

        assertThat(result, equalTo(Double.NEGATIVE_INFINITY));
    }

    @Test
    public void testGenerateAndGetComplexNumbers() {
        InderFormsGetter inderForms = new InderFormsGetter();

        List<Double> result = inderForms.generateAndGetComplexNumbers();

        assertThat(result, everyItem(equalTo(Double.NaN)));
    }
}

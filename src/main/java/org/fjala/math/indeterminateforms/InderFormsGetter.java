package org.fjala.math.indeterminateforms;

import com.google.common.collect.ImmutableList;

import java.util.Collections;
import java.util.List;

public class InderFormsGetter {

    public double getFirstForm() {
        return 0d / 0d;
    }

    public double getSecondForm() {
        return Double.POSITIVE_INFINITY / Double.POSITIVE_INFINITY;
    }

    public double getThirdForm() {
        return 0 * Double.POSITIVE_INFINITY;
    }

    public double getFourthForm() {
        return Math.pow(1, Double.POSITIVE_INFINITY);
    }

    public double getFifthForm() {
        return pow(0, 0);
    }

    public double getSixthForm() {
        return pow(Double.POSITIVE_INFINITY, 0);
    }

    public double getSeventhForm() {
        return Double.POSITIVE_INFINITY + Double.NEGATIVE_INFINITY;
    }

    public double generateAndGetPositiveInfinity() {
        return 123d / 0d;
    }

    public double generateAndGetNegativeInfinity() {
        return -123d / 0d;
    }

    public List<Double> generateAndGetComplexNumbers() {
        return ImmutableList.of(
                0d / 0d,
                Math.sqrt(-1),
                Math.sqrt(-6),
                Double.NaN + 1,
                Double.NaN / Double.NaN,
                Double.NaN * 2);
    }

    /**
     * The method returns the result of the pow method of the Math class but this one presents
     * differences with respect to the indeterminate of calculation.
     *
     * @param a The base.
     * @param b The exponent.
     * @return a to the b power.
     */
    private double pow(final double a, final double b) {
        if (!areFinite(a, b) || areBothZero(a, b)) {
            return Double.NaN;
        }
        return Math.pow(a, b);
    }

    private boolean areFinite(double a, double b) {
        return Double.isFinite(a) && Double.isFinite(b);
    }

    private boolean areBothZero(double a, double b) {
        return a == 0 && b == 0;
    }
}
